const initialState = {
    historicFilms: []
  }
  
  function manageHistoricFilms(state = initialState, action) {
    let newState
    switch (action.type){
      case 'TOGGLE_FILMDETAIL':
      const index = state.historicFilms.findIndex(item=>item.id === action.value.id)
      if (index === -1)
        return {...state, historicFilms:[...state.historicFilms, action.value]}
      else
        return state
      case 'REMOVE_HISTORIC_FILM':
      let { historicFilms } = state
      historicFilms.filter(film => film.id !== action.value.id)
        return {...state, historicFilms}
      case 'RESET_HISTORIC':
        return {...state, historicFilms:[]}
      default:
    }
    return state
  }
  
  export default manageHistoricFilms
  