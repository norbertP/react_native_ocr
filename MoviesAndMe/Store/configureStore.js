import { applyMiddleware, createStore } from 'redux';
import toggleFavorite from './Reducers/favoriteReducer'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const rootPersistConfig = {
  key: 'root',
  storage: storage
}

const logger = createLogger({
  // ...options
})
const _composeWithDevTools = composeWithDevTools(applyMiddleware(logger))
export default createStore(
    persistCombineReducers(rootPersistConfig, 
        {
        toggleFavorite}
        ),
        _composeWithDevTools)

