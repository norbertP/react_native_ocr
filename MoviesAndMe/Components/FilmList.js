// Components/FilmList.js

import React from 'react'
import { FlatList, StyleSheet, View, TextInput, Button, Text, ActivityIndicator } from 'react-native'
import FilmItem from './FilmItem'
import { getFilmsFromApiWithSearchedText } from '../API/TMDBAPI'
import {connect} from 'react-redux'


class FilmList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false
        }
   }
 

   displayDetailForFilm = (idFilm) => {
        this.props.navigation.navigate("FilmDetail", { idFilm })
        console.log("demande de vue du film : " + idFilm)
    }

    render() {
        console.log("RENDER FilmList", this.props)
        
        return (
            <View style={styles.main_container}>
                {!this.state.isLoading && <FlatList
                    data={this.props.films}
                    keyExtractor={(item) => item.id.toString()}
                    extraData={this.props.extraData}
                    renderItem={({ item }) => {
                        const isFavoriteFilm = (this.props.favoritesFilm.findIndex(favorite => favorite.id === item.id) !== -1)
                        return <FilmItem film={item} displayDetailForFilm={this.displayDetailForFilm} isFavoriteFilm={isFavoriteFilm} />
                    }}
                    onEndReachedThreshold={0.5}
                    onEndReached={() => { if (this.props.hasDynamicLoading && this.props.page < this.props.totalPages) this.props.loadFilms() }
                    }

                />}
                {this.state.isLoading && <View style={styles.spin}>
                    <ActivityIndicator size="large" />
                </View>}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    textinput: {
        marginLeft: 5,
        marginRight: 5,
        height: 50,
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5
    },
    spin: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

function mapStateToProp(state){
    return {favoritesFilm : state.toggleFavorite.favoritesFilm}
  }
  
export default connect(mapStateToProp)(FilmList)
