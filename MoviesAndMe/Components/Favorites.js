// Components/Search.js

import React from 'react'
import { FlatList, StyleSheet, View, TextInput, Button, Text, ActivityIndicator } from 'react-native'
import FilmList from './FilmList'
import { getFilmsFromApiWithSearchedText } from '../API/TMDBAPI'
import { connect } from 'react-redux'
import FadeIn from '../Animations/FadeIn'
//import Avatar from './Avatar'

class Favorites extends React.Component {
    constructor(props) {
        super(props)
        this.searchedText = ""
        this.page = 0 // Compteur pour connaître la page courante
        this.totalPages = 0 // Nombre de pages totales pour savoir si on a atteint la fin des retours de l'API TMDB

        this.loadFilms = this.loadFilms.bind(this)

    }
    loadFilms() { }
    loadFilms2() {
        if (this.searchedText !== '') {
            this.setState({ isLoading: true });
            getFilmsFromApiWithSearchedText(this.searchedText, this.page + 1).then(data => {
                this.page = data.page
                this.totalPages = 3;//data.total_pages
                console.log('total_pages', data.total_pages)
                this.setState({
                    films: [...this.state.films, ...data.results],
                    isLoading: false
                })
            })
        }
    }

    displayDetailForFilm = (idFilm) => {
        this.props.navigation.navigate("FilmDetail", { idFilm })
        console.log("demande de vue du film : " + idFilm)
    }



    render() {

        return (
            <View style={styles.main_container}>
                <View style={styles.avatar_container}>
                </View>
                <FilmList
                    films={this.props.favoritesFilm}
                    extraData={this.props.favoritesFilm}
                    navigation={this.props.navigation}
                    hasDynamicLoading={false}

                />
            </View>

        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    textinput: {
        marginLeft: 5,
        marginRight: 5,
        height: 50,
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5
    },
    spin: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar_container: {
        alignItems: 'center'
      }
})

function mapStateToProp(state) {
    return { favoritesFilm: state.toggleFavorite.favoritesFilm }
}

export default connect(mapStateToProp)(Favorites)
