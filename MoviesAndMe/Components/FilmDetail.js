// Components/FilmDetail.js

import React from 'react'
import { Animated, StyleSheet, Platform, View, Text, TouchableOpacity, ActivityIndicator, ScrollView, Image, Share } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { getFilmDetailFromApi, getImageFromApi } from '../API/TMDBAPI'
import { connect } from 'react-redux'
import moment from 'moment'
import numeral from 'numeral'
import EnlargeShrink from '../Animations/EnlargeShrink'

class FilmDetail extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state
    // On accède à la fonction shareFilm et au film via les paramètres qu'on a ajouté à la navigation
    if (params.film != undefined && Platform.OS === 'ios') {
      return {
        // On a besoin d'afficher une image, il faut donc passe par une Touchable une fois de plus
        headerRight: <TouchableOpacity
          style={styles.share_touchable_headerrightbutton}
          onPress={() => params.shareFilm()}>
          <Image
            style={styles.share_image}
            source={require('../Images/ic_share.png')} />
        </TouchableOpacity>
      }
    }
  }
  constructor(props) {
    super(props)
    this.state = {
      film: undefined,
      isLoading: true
    }
    this._shareFilm = this._shareFilm.bind(this)
  }
  // Fonction pour faire passer la fonction _shareFilm et le film aux paramètres de la navigation. Ainsi on aura accès à ces données au moment de définir le headerRight
  _updateNavigationParams() {
    this.props.navigation.setParams({
      shareFilm: this._shareFilm,
      film: this.state.film
    })
  }
  // Dès que le film est chargé, on met à jour les paramètres de la navigation (avec la fonction _updateNavigationParams) pour afficher le bouton de partage
  componentDidMount() {
    const favoriteFilmIndex = this.props.favoritesFilm.findIndex(item => item.id === this.props.navigation.state.params.idFilm)
    if (favoriteFilmIndex !== -1) {
      this.setState({isLoading: false,
        film: this.props.favoritesFilm[favoriteFilmIndex]
      }, () => { this._updateNavigationParams() })
      return
    }

    this.setState({ isLoading: true })
    getFilmDetailFromApi(this.props.navigation.state.params.idFilm).then(data => {
      this.setState({
        film: data,
        isLoading: false
      }, () => { this._updateNavigationParams() })
    })
  }

  componentDidUpdate() {
    console.log("componentDidUpdate : ")
    console.log(this.props.favoritesFilm)
  }

  _shareFilm() {
    const { film } = this.state
    Share.share({ title: "Partage du film : " + film.title, message: film.overview })
  }

  _toggleFavorite() {
    // Définition de notre action ici
    const action = { type: "TOGGLE_FAVORITE", value: this.state.film }
    this.props.dispatch(action)
  }

  _displayFloatingActionButton() {
    const { film } = this.state
    if (film != undefined && Platform.OS === 'android') { // Uniquement sur Android et lorsque le film est chargé
      return (
        <TouchableOpacity
          style={styles.share_touchable_floatingactionbutton}
          onPress={() => this._shareFilm()}>
          <Image
            style={styles.share_image}
            source={require('../Images/ic_share.png')} />
        </TouchableOpacity>
      )
    }
  }

  _displayFavoriteImage() {
    var sourceImage = require('../Images/ic_favorite_border.png')
    var shouldEnlarge = false // Par défaut, si le film n'est pas en favoris, on veut qu'au clic sur le bouton, celui-ci s'agrandisse => shouldEnlarge à true
    if (this.props.favoritesFilm.findIndex(item => item.id === this.state.film.id) !== -1) {
      sourceImage = require('../Images/ic_favorite.png')
      shouldEnlarge = true // Si le film est dans les favoris, on veut qu'au clic sur le bouton, celui-ci se rétrécisse => shouldEnlarge à false
    }
    return (
      <EnlargeShrink
        shouldEnlarge={shouldEnlarge}>
        <Image
          style={styles.favorite_image}
          source={sourceImage}
        />
      </EnlargeShrink>
    )
  }

  _displayFilm() {
    const film = this.state.film
    if (film) {
      // Si isLoading vaut true, on affiche le chargement à l'écran
      return (
        <ScrollView style={styles.scrollview_container}>
          <Image
            style={styles.image}
            flex={1}
            source={{ uri: getImageFromApi(film.backdrop_path) }}
          />
          <Text style={styles.title}>{film.title}</Text>
          <TouchableOpacity
            style={styles.favorite_container}
            onPress={() => this._toggleFavorite()}>
            {this._displayFavoriteImage()}
          </TouchableOpacity>
          <Text style={styles2.overview}>{film.overview}</Text>
          <View style={styles2.detail_item}>
            <Text>Sorti le {moment(film.release_date, "YYYY-MM-DD").format('DD/MM/YYYY')}</Text>
            <Text>Note : {film.vote_average} / 10</Text>
            <Text>Nombre de votes : {film.vote_count}</Text>
            <Text>Budget : {numeral(film.budget).format('0,0 $')}</Text>
            <Text>Genres : {film.genres.map(genre => genre.name).join(' / ')}</Text>
            <Text>Compagnies : {film.production_companies.map(genre => genre.name).join(' / ')}</Text>
          </View>
        </ScrollView>
      )
    }
  }
  //          <Text style={styles.detail_item}>Genres : {film.genres.reduce((res, {name})=>res?res+' / '+name:name)}</Text>
  _displayLoading() {
    if (this.state.isLoading) {
      // Si isLoading vaut true, on affiche le chargement à l'écran
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  render() {
    console.log("Les props", this.props);
    return (
      <View style={styles.main_container}>
        {this._displayFilm()}
        {this._displayLoading()}
        {this._displayFloatingActionButton()}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  main_container: {
    flex: 1,
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  scrollview_container: {
    flex: 1
  },
  favorite_container: {
    alignItems: 'center', // Alignement des components enfants sur l'axe secondaire, X ici
  },
  favorite_image:{
    flex: 1,
    width: null,
    height: null
  },
  share_touchable_floatingactionbutton: {
    position: 'absolute',
    width: 60,
    height: 60,
    right: 30,
    bottom: 30,
    borderRadius: 30,
    backgroundColor: '#e91e63',
    justifyContent: 'center',
    alignItems: 'center'
  },
  share_image: {
    width: 30,
    height: 30
  },
  image: {
    height: 169,
    margin: 5
  },

})
const styles2 = EStyleSheet.create({
  detail_item: {
    fontSize: '1rem',
    fontWeight: '400',
    paddingVertical: 2,
  },
  overview: {
    fontSize: '1rem',
    padding: 2,
    flexWrap: 'wrap',
    textAlign: 'left',
    fontStyle: "italic",
    fontWeight: '100',
    color: 'darkgrey'
  },
  title: {
    backgroundColor: 'whitesmoke',
    fontSize: '3rem',
    padding: 10,
    flexWrap: 'wrap',
    textAlign: 'center',
    fontWeight: "bold"
  },
})

const mapStateToProps = (state) => {
  return { favoritesFilm: state.toggleFavorite.favoritesFilm }
}

export default connect(mapStateToProps)(FilmDetail)