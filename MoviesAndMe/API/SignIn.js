import React, { Component } from 'react';
//import { SF_OAUTH_URL, REMOTE_ACCESS_CONSUMER_KEY } from 'react-native-dotenv';
import { AsyncStorage, Button, View, Text } from 'react-native';
import * as AuthSession from 'expo-auth-session';
//import { globalStyles } from '../constants/Styles';
//import ScreenKeys from '../constants/ScreenKeys';
const ScreenKeys = {};
const globalStyles = { container:{marginTop:100}};


const clientId = '8d7afd0fb4aea55';
const clientSecret = '10ab08281c56e2590ad5d42d2e913ac4c72aeced';
const SF_OAUTH_URL = 'https://api.imgur.com/oauth2/authorize';
const REMOTE_ACCESS_CONSUMER_KEY = clientId

export default class SignInScreen extends Component {
  static navigationOptions = {
    title: 'Please sign in',
  };

  state = {
    errorCode: null,
  };

  _signInAsync = async () => {
    const { navigation } = this.props;
    const redirectUrl = AuthSession.getRedirectUrl();
    console.log("test", redirectUrl);
    const result = await AuthSession.startAsync({
      authUrl:
        `${SF_OAUTH_URL}` +
        `?response_type=token` +
        `&client_id=${REMOTE_ACCESS_CONSUMER_KEY}` +
        `&redirect_uri=${encodeURIComponent(redirectUrl)}`
    });//, returnUrl: 'moviesandme://callback' });
    console.log(result);
    const { type, errorCode = 'You cancel or dismissed the login' } = result;
    if (type === 'success') {
      // Just simple way to store the token in this examples
      await AsyncStorage.setItem('userToken', JSON.stringify(result));
      console.log("Ca marche !!!!!")
      navigation.navigate(ScreenKeys.main);
    } else {
      /**
       * Result types can be: cancel, dismissed or error (with errorCode)
       */
      this.setState({ errorCode });
    }
  };

  render() {
    const { errorCode } = this.state;
    return (
      <View style={globalStyles.container}>
        <Button title="Sign in!" onPress={this._signInAsync} />
        {errorCode ? <Text>{errorCode}</Text> : null}
      </View>
    );
  }
}